# CONTRIBUTING

- Follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
    - References:
      - https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716
      - https://ec.europa.eu/component-library/v1.15.0/eu/docs/conventions/git/
- Follow Go bet practises
  - https://google.github.io/styleguide/go/best-practices.html
  - https://golangdocs.com/golang-best-practices
- Test naming convention
  - Describing what the SUT (System Under Test) does. It will also force you to test one thing at a time as you don’t want more than one condition in the requirement e.g. TestReturnsBadRequestWhenTheBodyIsEmpty.
  - https://medium.com/getground/naming-tests-in-golang-c58c188bb9a1
